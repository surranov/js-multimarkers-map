/*
 * Создание карты
 */
var map;
function initMap() {

  // инициализация карты
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 56.320422, lng: 44.013856},
    zoom: 15,
    disableDefaultUI: false,
    scrollwheel: true,
    draggable: true
  });


  /*
   * Сбор данных об объектах из html
   *
   * @param mapMarkers массив данных о всех объектах
   */
  function collectMarkersData (items) {
    var item;
    var mapMarkers = [];
    var mapMarker = {};

    for (var i = 0; i < items.length; i++) {
      item = items.eq(i);
      mapMarker = {};
      mapMarker.id = item.attr('data-id');
      mapMarker.lat = item.attr('data-lat');
      mapMarker.lng = item.attr('data-lng');
      mapMarker.img = item.find('.homes-list_img').attr('src');
      mapMarker.title = item.find('.homes-list_item-title').html();
      mapMarker.desc = item.find('.homes-list_item-status-text').html();
      mapMarker.price = item.find('.homes-list_item-price').html();
      mapMarkers.push(mapMarker);
    }
    return mapMarkers;
  }
  var mapMarkers = collectMarkersData( $('.homes-list_item') );


  /*
   * Создание маркеров и инфоокон
   *
   * @param marker переменная для текущего маркера
   * @param markers массив всех маркеров
   * @param infowindows массив всех инфоокон
   * @param bounds параметры для зума и расширения карты
   */
  var marker;
  var markers = [];
  var infowindows = [];
  var bounds = new google.maps.LatLngBounds();

  // создаем маркеры для каждого элемента
  // массива mapMarkers
  for (var i = 0; i < mapMarkers.length; i++) {
    // размещение маркеров на карте
    marker = new google.maps.Marker({
      position: {lat: +mapMarkers[i].lat, lng: +mapMarkers[i].lng},
      map: map,
      icon: {url: 'assets/img/listing-map/pin.png'},
    });
    // накапливаем массив маркеров для плагина markerClusterer
    markers.push(marker);
    // расширение карты до зоны маркера
    bounds.extend(marker.position);
    // создание отдельного инфоокна для маркера
    var infowindowContent = '<div class="infowindow"><div class="infowindow_img-wrap" style="background-image: url(' + mapMarkers[i].img + ')"></div><div class="infowindow_desc"><div class="infowindow_desc-top text--400-14">' + mapMarkers[i].title + '</div><div class="infowindow_desc-bottom"><span class="infowindow_subtitle">' + mapMarkers[i].desc + '</span><span class="infowindow_price">' + mapMarkers[i].price + '</span></div></div></div>';
    makeInfoWin(marker, infowindowContent, map);
  }

  // инициализация расширения карты
  // с помощью накопленных данных bounds
  map.fitBounds(bounds);
  // объединение маркеров в группы с помощью плагина markerClusterer.js и
  // ранее сформированного массива маркеров markers
  var markerClusterer;
  markerClusterer = new MarkerClusterer(map, markers);


  /*
   * Эта и последующие функции размещены внутри функции initMap
   * для общей области видимости переменных
   *
   * Создание инфоокон
   *
   * @param marker маркер к которому крепится текущее инфоокно
   * @param content разметка текущего инфоокна
   * @param map карта к которой крепятся события
   */
  function makeInfoWin(marker, content, map) {
    var infowindow = new google.maps.InfoWindow({ content: content });
    // накапливаем массив инфоокон для работы с ними
    infowindows.push(infowindow);

    // открываем инфоокно по клику
    google.maps.event.addListener(marker, 'click', function() {
      closeAllInfowindows();
      infowindow.open(map,marker);
      removeInfowindowStyle();
    });
    // закрытие инфоокна, когда изменяется зум
    google.maps.event.addListener(map, 'zoom_changed', function() {
      infowindow.close();
    });
    // закрытие инфоокна, при клике на карту
    google.maps.event.addListener(map, 'click', function() {
      infowindow.close();
    });
  }


  /*
   * Закрытие всех инфоокон
   * 
   * массив всех инфоокон накапливается в
   * переменной infowindows
   */
  function closeAllInfowindows() {
    for (var i=0; i < infowindows.length; i++) {
       infowindows[i].close();
    }
  }


  /*
   * Удаление стандартных стилей инфоокна
   * 
   * все стили инфоокна присвоены к 2м элементам
   */
  function removeInfowindowStyle() {
    $(document).find('.gm-style-iw').prev().css('display','none');
    $(document).find('.gm-style-iw').next().css('display','none');
  }
}